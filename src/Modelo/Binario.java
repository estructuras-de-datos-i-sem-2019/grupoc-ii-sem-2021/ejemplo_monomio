/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madarme
 */
public class Binario {
    
    private ListaCD<Bit> bits=new ListaCD();

    public Binario() {
    }
    
    /**
     *  Recibe un decimal y convierte a su correspondiente binario
     * Ejemplo:
     *  Si decimal=29
     * bits=<true, true, true, false, true> --> bits.toString()-->11101
     * @param decimal un valor decimal
     */
    public Binario(int decimal) {
    }

    public ListaCD<Bit> getBits() {
        return bits;
    }

    public void setBits(ListaCD<Bit> bits) {
        this.bits = bits;
    }

    @Override
    public String toString() {
        
            // :)
        return "";
        
    }
    
    /**
     *  NO SE DEBE USAR GET() 
     *  TODAS LAS OPERACIONES SON A NIVEL DE BINARIOS
     * 
     * Ver vídeo de la suma: https://www.youtube.com/watch?v=2WtqivPA4tk
     */
    
    /**
     * Realiza la suma binaria del original con numero 2
     * @param numero2 un objeto de la clase que representa un Binario
     * @return un objeto de la clase Binario
     */
    public Binario getSuma(Binario numero2)
    {
        return null;
    }
    
    
    /**
     * Realiza la resta binaria del original con numero 2
     * (usando complemento a 2)
     * @param numero2 un objeto de la clase que representa un Binario
     * @return un objeto de la clase Binario
     */
    public Binario getResta(Binario numero2)
    {
        return null;
    }
    /**
     * Obtiene el decimal a partir de la lista de bits
     * @return un entero que representa el número binario almacenado
     */
    public int getDecimal()
    {
       return 0;
    }
    
}
