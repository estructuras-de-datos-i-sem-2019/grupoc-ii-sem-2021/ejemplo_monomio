/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

/**
 *
 * @author madar
 */
public class TestGenerico {
    
    
    public static void main(String[] args) {
        
        String v[]={"madarme","juanpablo","edna", "Luiscarlos"};
        Integer v2[]={1,2,3,4,5,6};
        Float v3[]={4.5F, 8.3F,9.01F};
        imprimirVector(v);
        imprimirVector(v2);
        imprimirVector(v3);
        
    }
    /**
     * Método generico
     * @param <T> tipo de dato
     * @param vector un vector de cualquier tipo
     */
    private static <T> void imprimirVector(T vector[])
    {
        System.out.println("-------------------\n");
        for(T dato:vector)
            System.out.print(dato.toString()+"\t");
        
    }
    
}
